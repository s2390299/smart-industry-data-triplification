from rdflib import Graph, Namespace, URIRef, Literal
from rdflib.namespace import RDF, XSD
import pandas as pd
import sys

def main(input_path):
    # Load dataset
    data = pd.read_csv(input_path, index_col=0, parse_dates=True)
    data = data.drop(['cet_cest_timestamp', 'interpolated'], axis=1)

    # Define namespaces
    SAREF = Namespace("https://w3id.org/saref#")
    EX = Namespace("http://example.org/Jilmer/")

    # Create RDF graph and bind namespaces
    graph = Graph()
    graph.bind("saref", SAREF)
    graph.bind("ex", EX)

    unit_kwh = URIRef(EX['Units/kWh'])

    # Process each row in the dataset
    for timestamp, row in data.iterrows():
        timestamp_uri = URIRef(EX[f'timestamps/{timestamp.isoformat()}'])

        for column_name, value in row.items():
            if pd.notna(value):
                building, device = column_name.rsplit('_', 2)[0], '_'.join(column_name.rsplit('_', 2)[1:])
                
                device_uri = URIRef(EX[f'Devices/{building}/{device}'])
                building_uri = URIRef(EX[f'Buildings/{building}'])
                measurement_uri = URIRef(EX[f'Measurements/{building}/{device}/{timestamp.isoformat()}'])

                # Add triples to the graph
                graph.add((measurement_uri, RDF.type, SAREF.Measurement))
                graph.add((measurement_uri, SAREF.hasTimestamp, Literal(timestamp.isoformat(), datatype=XSD.dateTime)))
                graph.add((measurement_uri, SAREF.isMeasuredIn, unit_kwh))
                graph.add((measurement_uri, SAREF.hasValue, Literal(float(value), datatype=XSD.float)))
                graph.add((measurement_uri, EX['is_measurement_Of'], device_uri))

                graph.add((device_uri, RDF.type, SAREF.Device))
                graph.add((device_uri, EX['has_measurement'], measurement_uri))
                graph.add((device_uri, SAREF.isContainedIn, building_uri))

                graph.add((building_uri, RDF.type, SAREF.Building))
                graph.add((building_uri, SAREF.Contains, device_uri))

    # Serialize the graph to a Turtle file
    graph.serialize(destination="graph.ttl", format="turtle")
    print(f"RDF graph has been serialized to 'graph.ttl'")

if __name__ == "__main__":
    input_file_path = sys.argv[1]
    main(input_file_path)
